# Modbus Slave Example
Example project of a Modbus slave client. This application will create 10 coils, 10 holding registers, 10 discrete inputs and 10 input registers.

For more information on how to connect this client and how to read/write information see:

[Modbus Broker](https://gitlab.com/modbus/modbus-broker)

[Modbus Slave](https://gitlab.com/modbus/modbus-slave)

## Install Dependencies
In order to run the modbus-slave-example project we must first install the modbus-slave dependency into the local maven repository. 
This can be be done by running the following commands in a folder somewhere on your computer.
```
git clone https://gitlab.com/modbus/modbus-slave.git
cd modbus-slave
gradle install
```

## Run
To start the modbus slave example application, run the following command:
```
git clone https://gitlab.com/modbus/modbus-slave-example
cd modbus-slave-example
gradle runApplication
```

## Build
To create a jar file run the following command:
```
gradle fatJar
```

The jar file can then be found at "build/libs/modbus-slave-example-all-1.0-SNAPSHOT.jar".
To run it, execute the following command:
```
java -jar modbus-slave-example-all-1.0-SNAPSHOT.jar
```

## License
MIT License

Copyright (c) 2018 Henrik

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
